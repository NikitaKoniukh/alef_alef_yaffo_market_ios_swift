//
//  ViewController.swift
//  newmarket
//
//  Created by Nikita Koniukh on 26/09/2019.
//  Copyright © 2019 nisancomp. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler {

    // MARK: - Outlets
    @IBOutlet weak var appLogoImageView: UIImageView!
    @IBOutlet weak var customerLogoImageView: UIImageView!
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var customerPhoneNumberLabel: UILabel!

    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var webView: WKWebView!

    
    //MARK: - Properties
    var marketNumber: String = ""
    var supllierName: String = ""
    var supllierPhone: String = ""
    var uuidNumber: String = ""
    var secureCode: String = ""
    var username: String = ""
    var password: String = ""
    var customerNumber: String = ""
    let imageBaseStringUrl = "http://nisancomp.co.il/media/"

    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.startAnimating()
        spinner.isHidden = false
        marketNumber = "1195"
        username = "95yasm"
        password = "bguzcp43"
        uuidNumber = (UIDevice.current.identifierForVendor?.uuidString)!
        hideKeyboardWhenTappedAround()
        DispatchQueue.main.async {
            self.setupView()
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
        }

        // WKwebView configuration
        webView.configuration.userContentController.add(self, name: "order_confirmed")
        self.webView.navigationDelegate = self
    }

    //MARK: - SetupView
    private func setupView() {
        webView.isHidden = true
        spinner.isHidden = false
        spinner.startAnimating()
        setupCustomerLogo()
        setupAppLogo()
        setupSupllierNameAndSuplierPhone()
        popupView.alpha = 0
        alertLabel.alpha = 0
        popupView.layer.cornerRadius = 8
    }

    private func setupCustomerLogo() {
        let imageStringUrl = "\(imageBaseStringUrl)\(marketNumber)/logo.jpg"
        guard let imageUrl =  URL(string: imageStringUrl) else { return }
        do {
            let data = try Data(contentsOf: imageUrl)
            self.customerLogoImageView.image = UIImage(data: data)
        } catch let err {
            print(err.localizedDescription)
        }
    }

    private func setupAppLogo() {
        let imageStringUrl = "https://www.new-market.co.il/images/logo.png"
        guard let imageUrl =  URL(string: imageStringUrl) else { return }
        do {
            let data = try Data(contentsOf: imageUrl)
            self.appLogoImageView.image = UIImage(data: data)
        } catch let err {
            print(err.localizedDescription)
        }
    }

    private func setupSupllierNameAndSuplierPhone() {
        let stringUrl = "https://nisancomp.co.il/get_supplier_name1.php?v2=yes&m="
        guard let supllierStringUrl = URL(string: "\(stringUrl)\(marketNumber)") else { return }
        let request = URLRequest(url: supllierStringUrl)
        let task = URLSession.shared.dataTask(with: request) { (data, resp, err) in
            if let err = err {
                print("Error: \(err)")
            } else {
                if let response = resp as? HTTPURLResponse {
                    print("Status code: \(response.statusCode)")
                }
            }

            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                let tempStringArray = dataString.split(separator: "#")
                self.supllierName = String(tempStringArray[0])
                self.supllierPhone = String(tempStringArray[1])
                DispatchQueue.main.async {
                    self.spinner.isHidden = true
                    self.spinner.stopAnimating()
                    self.enterButton.setTitle("כניסה להזמנות של \n\(self.supllierName)", for: .normal)
                    self.customerPhoneNumberLabel.text = self.supllierPhone
                    self.customerPhoneNumberLabel.alpha = 1
                }
            }
        }
        task.resume()
    }

    // MARK: - Actions
    @IBAction func enterButtonWasPressed(_ sender: UIButton) {
        if UserDefaults.standard.string(forKey: "loggedIn") == "loggedIn" {
            checkDeviceHttpRequest()
        } else {
            self.popupView.alpha = 1
            self.enterButton.alpha = 0
        }
    }

    @IBAction func confirmButtonWasPressed(_ sender: UIButton) {
        if codeTextField.text != "" {
            if UserDefaults.standard.string(forKey: "registered") == "registered"{
                checkDeviceHttpRequest()
            } else {
                registerDeviceHttpRequest()
            }
        }
    }

    @IBAction func cancelButtonWasPressed(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5) {
            self.popupView.alpha = 0
            self.enterButton.alpha = 1
            self.hideKeyboard()
        }
    }

    // MARK: - Web requests
    private func registerDeviceHttpRequest() {
        let baseStringUrl = "http://nisancomp.co.il/activate_device_id1.php?op_code1=register_device_id1&m="
        guard let secureCode = codeTextField.text else { return }
      //  let fakeUuidNumber = "D08CC0A5-E346-4CDF-82DE-91D4E41E9B13"
        guard let urlString = URL(string: "\(baseStringUrl)\(marketNumber)&ms=\(uuidNumber)&secure_code1=\(secureCode)") else { return }

        var request = URLRequest(url: urlString)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data, resp, err) in
            if let err = err {
                print("Error: \(err)")
            } else {
                if let response = resp as? HTTPURLResponse {
                    print("Status code: \(response.statusCode)")

                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
                        if dataString == "no_exist" {
                            DispatchQueue.main.async {
                                UIView.animate(withDuration: 0.4, animations: {
                                    self.alertLabel.alpha = 1
                                    self.hideKeyboard()
                                })
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.popupView.alpha = 0
                                let tempStringArray = dataString.split(separator: "=")
                                self.customerNumber = String(tempStringArray[1])
                                self.hideKeyboard()
                                UserDefaults.standard.set("registered", forKey: "registered")
                                UserDefaults.standard.set("loggedIn", forKey: "loggedIn")
                                self.webView.isHidden = false
                                self.addMainWebView()
                            }
                        }
                    }
                }
            }
        }
        task.resume()
    }

    private func checkDeviceHttpRequest() {
        let baseStringUrl = "http://nisancomp.co.il/activate_device_id1.php?op_code1=check_device_id1&m="
        guard codeTextField.text != nil else { return }
        //let fakeUuidNumber = "D08CC0A5-E346-4CDF-82DE-91D4E41E9B13"
        guard let urlString = URL(string: "\(baseStringUrl)\(marketNumber)&ms=\(uuidNumber)") else { return }

        var request = URLRequest(url: urlString)
        let checkUrlString = ".op_code1=check_device_id1&m=\(marketNumber)"

        request.httpMethod = "POST"

        let data = checkUrlString.data(using: .utf8)
        request.httpBody = data

        let task = URLSession.shared.dataTask(with: request) { (data, resp, err) in
            if let err = err {
                print("Error: \(err)")
            } else {
                if let response = resp as? HTTPURLResponse {
                    print("Status code: \(response.statusCode)")

                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
                        if dataString == "no_exist" {
                            DispatchQueue.main.async {
                                UIView.animate(withDuration: 0.4, animations: {
                                    self.alertLabel.alpha = 1
                                    self.hideKeyboard()
                                })
                            }
                        } else {
                            DispatchQueue.main.async {
                                UserDefaults.standard.set("loggedIn", forKey: "loggedIn")
                                self.popupView.alpha = 0
                                let tempStringArray = dataString.split(separator: "=")
                                self.customerNumber = String(tempStringArray[1])
                                self.hideKeyboard()
                                self.webView.isHidden = false
                                self.addMainWebView()
                            }
                        }
                    }
                }
            }
        }
        task.resume()
    }

    // MARK: - WKWebView
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }

    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        setupView()
        self.enterButton.alpha = 1
    }

    private func addMainWebView() {
        let urlString = "submit=yes&enter_mobile1=1&username=\(self.username)&password=\(self.password)&customer_num1=\(self.customerNumber)"

        var loginStringUrl = "http://nisancomp.co.il/m_login_ios1.php?"
        loginStringUrl.append(urlString)

        guard let url = URL(string: loginStringUrl) else { return }
        var request = URLRequest(url: url)

        request.httpMethod = "POST"

        var body = Data()
        body.append(Data(loginStringUrl.utf8))
        request.httpBody = body

        webView.load(request)
    }
}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}
